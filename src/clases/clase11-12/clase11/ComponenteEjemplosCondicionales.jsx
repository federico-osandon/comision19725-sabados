import { useState, useEffect } from "react";


 
export const ControlledInput = () => {
    const [input, setInput] = useState("");
    console.log(input);
    return (
      <input
        type="text"
        value={input}
        onInput={(evet) => setInput(evet.target.value)}
      />
    );
  };


  
export  function LoadingComponent() {
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {

      setTimeout(() => {
        setLoading(false);
      }, 5000);
      return ()=>{
          console.log('Limpiando componente');
      }
    }, []);
    
    return <>
        {loading ? <h2>Loading... </h2> : <h2>Productos cargardos!</h2>}
    </>;
  }


  
export  function TextComponent({ condition = false }) {
    
    if (!condition) {
      return <h2>Uds no esta logeado</h2>;
    }
  
    return <h2>Ud esta logueado puede ver la pág.</h2>;
  }



export  function TextComponent2({ condition = true }) {
    
    return (
      <>
        {condition && <h2>Ud esta logueado puede ver la pág.</h2>}

        {!condition && <h2>Ud no esta logueado, NO puede ver la pág.</h2>}

      </>
    );
  }



export  function TextComponent3({ condition = false }) {
    return (
      <>
        <h2>{condition ? 'Ud esta logueado puede ver la pág.' : 'Ud esta logueado puede ver la pág.'}</h2>            
        
      </>
    )
  }



 export function TextComponent4({ condition = false }) {

    return (
      <>
        <h2 style={ { color: condition ? "green" : "red" } }>
          No hay stock.
        </h2>
      </>
    );
  }


  
export  function TextComponent5({ condition = false }) {
    return (
      <>
        <h2 className={ (condition === true) ? "btn btn-success" : "btn btn-danger" }>
          agregar al carrtio
        </h2>
      </>
    );
  }






export  function TextComponent6({ condition = true, otro }) {
    return (
      <>
        <h2
          className={ `${condition === true ? "btn btn-success" : "btn btn-danger"} ${otro || ""} `}
        >
          Ud esta logueado puede ver la pág.
        </h2>
      </>
    );
  }




export function TextComponent7({ condition = true , otro='mt-5' }) {
    
    const config = condition
        ?
            {
                className: `btn btn-success ${otro ||  ""}`,
                style: {color: 'red'},
                title: "Este es el titulo si la condicion es verdadera",
                nombre: 'Fede'
            }
        : 
            {}

        return (
            <>
                <h2 {...config} >Ud esta logueado puede ver la pág.</h2>
            </>
    )
  }
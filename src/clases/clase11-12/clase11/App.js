import { ControlledInput, 
         LoadingComponent,
         TextComponent,
         TextComponent2,
         TextComponent3,
         TextComponent4,
         TextComponent5,
         TextComponent6,
         TextComponent7
        } from "./ComponenteEjemplosCondicionales";
import "bootstrap/dist/css/bootstrap.min.css";




export default function App() {
  return (
        <div className="App">
            <h1>Ejemplos de clase 11</h1>
            
            {/* <ControlledInput /> */}
            {/* <LoadingComponent /> */}
            {/* <TextComponent /> */}
            {/* <TextComponent2 /> */}
            {/* <TextComponent3 /> */}
            <TextComponent4 />
            <TextComponent5 /><br/>
            <TextComponent6 otro='mt-2' /><br/>
            <TextComponent7 />
        </div>
  );
}


import { useEffect } from 'react'
import {NavBar} from './components/NavBar/NavBar'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import ItemList from './components/ItemList/ItemList';
import ItemListContainer from './components/containers/ItemListContainer';
import Cart from './components/Cart/Cart';
import ItemDetailContainer from './components/containers/ItemDetailContainer';

export default function App() {

    // function handleOnClick(){
    //     alert('soy evento del app')
    // }

    // useEffect(() => {
    //     function resizeHandler(e){
    //         console.log(e);
    //         console.log(e.clientX);
    //         console.log(e.clientY);            
    //     }
            
    //     window.addEventListener('mousemove', resizeHandler)

    //     return ()=>{
    //         window.removeEventListener(resizeHandler)
    //     }
        
    // },[])

    return (
        <div 
            //onClick = {handleOnClick}
            className='border border-3 border-primary'
        >
            <Router>
                <NavBar />

                <Switch>
                <Route path='/' exact component={ItemListContainer} />
                
                <Route path='/category/:idCategory' exact component={ItemListContainer} />
                
                <Route path='/detalle/:idProducto' exact component={ItemDetailContainer } />
                
                <Route exact path='/cart'>              
                    <Cart />
                </Route>

                </Switch>

            </Router>        
        </div>
    );
}


import ItemCount from "../../ItemCount/ItemCount"
import { useAppContext } from "../../../Context/AppContext"


function ItemDetail( props ) {
    const { producto} = props 
    
    const {agregarAlCarrito} = useAppContext()
   
    const onAdd=(cant)=>{
        console.log(cant)  
        agregarAlCarrito(producto, cant)      
    }

    return (
        <>
            <label>Soy el detalle</label>
            <div className='card w-50'>
                <div className="container">
                    <label>{producto.name}</label>
                </div>
                <div className="container">
                    <img  src={producto.url} className="w-25" />
                    <br/>
                    <label>{producto.descripcion}</label>
                </div>
                <div className="container">
                    <label>{producto.price}</label>
                </div>
            </div>
            <ItemCount initial={1} stock={5} onAdd={onAdd} />
        </>
    )
}

export default ItemDetail

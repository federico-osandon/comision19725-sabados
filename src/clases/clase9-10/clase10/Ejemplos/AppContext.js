import React, { createContext, useContext, useState } from 'react';

const AppContext = createContext();

export const useAppContext = () => useContext(AppContext);

const AppContextProvider = ({ children }) => {
  const [products, setProducts] = useState([
    {
      id: 1,
      nombre: 'Remera',
      precio: 1000
    },
    {
      id: 2,
      nombre: 'Zapatos',
      precio: 3000
    }
  ]);

  return (
    <AppContext.Provider
      value={{
        products
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default AppContextProvider;

import React from 'react';
import { useCartContext } from './CartContext';
import { useAppContext } from './AppContext';
import './style.css';

export default function Item({ product }) {
  const { addToCart } = useCartContext();
  const { products } = useAppContext();

  const handleClick = id => {
    const findProductInDB = products.find(prod => prod.id === id);
    console.log('DESDE ITEM: ', findProductInDB);

    if (!findProductInDB) {
      console.log('NO SE PUDO AGREGAR AL CARRITO!!');
      return;
    }

    addToCart(findProductInDB);
  };

  return (
    <div>
      <h4>{product.nombre}</h4>
      <p>{product.precio}</p>
      <button onClick={() => handleClick(product.id)}>
        Agregar al carrito
      </button>
    </div>
  );
}

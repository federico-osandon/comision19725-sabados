import React, { useState } from "react";
import "./style.css";
import Item from './Item';
import { useAppContext } from './AppContext'


export default function Home() {

  const { products } = useAppContext();

  return (
    <div>
      <h1>HOME</h1>
      {products.length !== 0 && products.map(
        product => <Item key={product.id} product={product}/>)}
      {products.length === 0 && <p>No hay productos :(</p>}
    </div>
  );
}
import { useState, useEffect } from 'react'

import { useParams } from "react-router-dom";

// import { Input } from '../../clases/clase9-10/clase9/Input';
import { getFirestore } from '../../sevices/getFirebase';

// import { getFetch } from '../../Ultil/getMock';

import ItemList from '../ItemList/ItemList';





function ItemListContainer({greeting}) { 

    const [personas, setPersonas] = useState([])//    
    const [item, setItem] = useState({})//    
    const [loading, setLoading] = useState(true)
    const [bool, setBool] = useState(true)

    const { idCategory } = useParams()


    useEffect(() => {

        const dbQuery = getFirestore()

        dbQuery.collection('items').get()
        .then(resp => {
            setPersonas( resp.docs.map(item => ( {id: item.id, ...item.data()} ) ) )
        })
        .catch(err => console.log(err))
        .finally(()=> setLoading(false))                   
        
    }, [idCategory])
        
    //ejemplo de evento
   const handleClick=(e)=>{
        e.preventDefault() 
        setBool(!bool)
    }

    const handleAgregar=()=>{
        setPersonas([
            ...personas,
            { id: "8", name: "Gorra 7", url: 'https://www.remerasya.com/pub/media/catalog/product/cache/e4d64343b1bc593f1c5348fe05efa4a6/r/e/remera_negra_lisa.jpg', categoria: "remera" , price: 2 }
        ])
    }

    console.log(personas);
    //console.log('itemListContainer');

    return (
        <div className='border border-3 border-primary'>             
            <h1> {greeting}</h1> 
         
            <button onClick={handleClick}>Cambiar estado </button> 
                      
            <button onClick={handleAgregar}>Agregar Item </button>
            { loading ? <h2>Cargando...</h2> :   <ItemList personas={personas} /> }              
          
        </div>
    )
}

export default ItemListContainer

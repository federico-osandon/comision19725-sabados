// import { Navbar, Container, Nav } from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import { useCartContext } from '../../Context/cartContext'


export const NavBar = ()=>{

    const { iconCart } = useCartContext()
    return <>
        <Navbar bg="light" expand="lg">
            <Container>
                <NavLink to='/' className="" activeClassName="" >
                    <Navbar.Brand >React-Bootstrap-Ecommerce</Navbar.Brand>
                </NavLink>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Link to='/category/gabinetes'>
                        <Nav.Link href="#link">Gabinetes</Nav.Link>
                    </Link>
                    <Link to='/category/teclados'>
                        <Nav.Link href="#link">Teclados</Nav.Link>
                    </Link>
                    <Nav.Link href="#link">Monitores</Nav.Link> 
                     
                    <Link to="/cart">
                        {iconCart()}
                        cart               
                    </Link>                  
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        </>

}


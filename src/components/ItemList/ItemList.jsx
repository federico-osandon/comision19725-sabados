import { memo } from 'react'
import Item from './Item'


// memo(()=>{}) =>  memo(ItemList)

// memo(()=>{}, fn) =>  memo(ItemList, condicional)


const ItemList = memo( ( {personas} ) => {
    
        console.log('soy itemList');
    
        return (
            <>
                { personas.map(persona=> <Item persona={persona} />     )}  
            </>
        )
    }
)

export default ItemList

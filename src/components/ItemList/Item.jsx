import { useContext } from 'react'
import { Link } from "react-router-dom"



const Item = ({persona}) => {

    console.log('soy item');

    return (       
            <div key={persona.id} className="card w-50 mt-5" >
                    <div className="card-header">
                        {persona.title}
                    </div>
                    <div className="card-body">
                        <img src={persona.imgaeID} alt="foto" />
                        {persona.age}
                       
                    </div>
                    <div className="card-footer">
                        <Link to = {`/detalle/${persona.id}`} >
                            <button className="btn btn-outline-primary btn-block">
                                detalle de persona
                            </button>
                        </Link>
                    </div>
                    
                </div>
        
    )
}

export default Item

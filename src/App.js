
import {useState, createContext, useContext } from 'react'

import {NavBar} from './components/NavBar/NavBar'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import ItemListContainer from './components/containers/ItemListContainer';
import Cart from './components/Cart/Cart';
import ItemDetailContainer from './components/containers/ItemDetailContainer';

import { CartContext } from './Context/cartContext';

import 'bootstrap/dist/css/bootstrap.min.css';



export const  ContextApp = createContext('Fede')//se crea una sola vez


export default function App() {
    const [state, setState] = useState('juan')

    function mostrarJuan(){
        alert(state)
    }
    
    

    return (

        <div            
            className='border border-3 border-primary'
        >
            
            <CartContext>
                <ContextApp.Provider value={{state, mostrarJuan}} >
                    
                    <Router>
                        <NavBar />

                        <Switch>
                        <Route path='/' exact component={ItemListContainer} />
                        
                        <Route path='/category/:idCategory' exact component={ItemListContainer} />
                        
                        <Route path='/detalle/:idProducto' exact component={ItemDetailContainer } />
                        
                        <Route exact path='/cart'>              
                            <Cart  />
                        </Route>

                        </Switch>

                    </Router> 
                </ContextApp.Provider>
            </CartContext>             
        </div>
    );
}

